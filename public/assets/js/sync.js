var datasync = {};


 
datasync.sync = function(){
  
    datasync.firstName       =           $('#firstName').val();
    datasync.lastName        =           $('#lastName').val();
    datasync.score           =           $('#form-score').val();
    datasync.gpa           =           $('#form-gpa').val();
   

    if (!datasync.firstName) {
        alert("Please provide a first name");
        return;
    }

    if (!datasync.lastName) {
        alert("Please provide a last name");
        return;
    }

    if (!datasync.score) {
        alert("Please provide your ACT Score");
        return;
    } 

    if (!datasync.score) {
        alert("Please provide your gpa");
        return;
    } 

    AWSCognito.config.region = 'us-east-1';
    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: COGNITO_IDENTITY_POOL_ID // your identity pool id here
    }); 

    //AWS.config.credentials.params.Logins = {};
    //AWS.config.credentials.params.Logins[<%= provider %>] = <%= token %> ;

    // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
    //AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})
    var syncManager;

    AWS.config.credentials.get(function(){
        syncManager = new AWS.CognitoSyncManager();
        syncManager.openOrCreateDataset('infoDataset', function(err, infoDataset) {
           infoDataset.put('firstname', datasync.firstName , function(err, record){
                
           });
           infoDataset.put('lastname', datasync.lastName , function(err, record){
          
           });
           infoDataset.put('score', datasync.score , function(err, record){
              console.log(record);
           });
           infoDataset.put('gpa', datasync.gpa , function(err, record){
          
           });

           infoDataset.synchronize({
              onSuccess: function(dataset, newRecords) {
                  
                  console.log(dataset);
                  alert("Data Submitted!");
                  //window.location = "/showData";              
                },



               onConflict: function(dataset, conflicts, callback) {
                  var resolved = [];

                   for (var i=0; i<conflicts.length; i++) {

                      // Take remote version.
                      //resolved.push(conflicts[i].resolveWithRemoteRecord());

                      // Or... take local version.
                       resolved.push(conflicts[i].resolveWithLocalRecord());

                      // Or... use custom logic.
                      // var newValue = conflicts[i].getRemoteRecord().getValue() + conflicts[i].getLocalRecord().getValue();
                      // resolved.push(conflicts[i].resovleWithValue(newValue);

                   }

                   dataset.resolve(resolved, function() {
                      return callback(true);
                   });

                   // Or... callback false to stop the synchronization process.
                   // return callback(false);

               },



               onDatasetDeleted: function(dataset, datasetName, callback) {
                  // Return true to delete the local copy of the dataset.
                   // Return false to handle deleted datasets outside the synchronization callback.

                   return callback(true);
               },

              onDatasetMerged: function(dataset, datasetNames, callback) {
                  // Return true to continue the synchronization process.
                   // Return false to handle dataset merges outside the synchroniziation callback.

                   return callback(false);
              },
              onFailure: function(err) {
                console.log(err);
                  alert(err);
               }
            });
    

          });

         
    });

}


datasync.fetchValues = function(){

    AWS.config.region = 'us-east-1';
    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: COGNITO_IDENTITY_POOL_ID // your identity pool id here
    }); 
    var p = $('#title').html(); 
    
    console.log('{{title}}');
    //AWS.config.credentials.params.Logins = {};
    //AWS.config.credentials.params.Logins[<%= provider %>] = <%= token %> ;


    // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
    //AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})
    var syncManager;

    AWS.config.credentials.get(function(){
        syncManager = new AWS.CognitoSyncManager();
        syncManager.openOrCreateDataset('infoDataset', function(err, dataset) {
           dataset.get('firstname', function(err, value) {
                if(value != null) {
                var textfname = document.getElementById('firstName');
                textfname.value=value;
            }
            });

           dataset.get('lastname', function(err, value) {
            if(value != null) {
                var textlname = document.getElementById('lastName');
                textlname.value=value;
            }
            });

           dataset.get('score', function(err, value) {
            if(value != null) {
                var textscore = document.getElementById('form-score');
                textscore.value=value;
            }
            });
           
           dataset.get('gpa', function(err, value) {
            if(value != null) {
                var textgpa = document.getElementById('form-gpa');
                textgpa.value=value;
            }
            });

          });

    
    });

    }
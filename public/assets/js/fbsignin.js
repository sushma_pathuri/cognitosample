var fbsignin = {};





fbsignin.login = function Login()
    {
 
 		FB.Event.subscribe('auth.authResponseChange', function(response) 
	    {
	     if (response.status === 'connected') 
	    {
	        document.getElementById("message").innerHTML +=  "<br>Connected to Facebook";
	        //SUCCESS
	 
	    }    
	    else if (response.status === 'not_authorized') 
	    {
	        document.getElementById("message").innerHTML +=  "<br>Failed to Connect";
	 
	        //FAILED
	    } else 
	    {
	        document.getElementById("message").innerHTML +=  "<br>Logged Out";
	 
	        //UNKNOWN ERROR
	    }
	    });

        FB.login(function(response) {
           if (response.authResponse) 
           {
           		console.log(response.authResponse.accessToken);
           		//console.log(response.authResponse.accessToken);

			           		// Add the Facebook access token to the Cognito credentials login map.
				var params = {
			        IdentityPoolId : COGNITO_IDENTITY_POOL_ID,
			        Logins: {
			            'graph.facebook.com' : response.authResponse.accessToken
			            			        }
			    };
			    AWS.config.region = 'us-east-1';
			    AWS.config.credentials = new AWS.CognitoIdentityCredentials(params);

			    // Obtain AWS credentials
			    AWS.config.credentials.get(function(err){
			         
			        if (err) return console.log("Error", err);
          			console.log("Cognito Identity Id", AWS.config.credentials.identityId);
			    });

                getUserInfo();
            } 
            else 
            {
             console.log('User cancelled login or did not fully authorize.');
            }
         },{scope: 'email'});
 
    }

  
function getUserInfo() {
        FB.api('/me?fields=name,email', function(response) {
 
      var str="<b>Name</b> : "+response.name+"<br>";
          str +="<b>Link: </b>"+response.link+"<br>";
          str +="<b>Username:</b> "+response.username+"<br>";
          str +="<b>token: </b>"+response.accessToken+"<br>";
          str +="<b>Email:</b> "+response.email+"<br>";
          str +="<input type='button' value='Get Photo' onclick='getPhoto();'/>";
          str +="<input type='button' value='Logout' onclick='Logout();'/>";
          console.log(str);
          //document.getElementById("status").innerHTML=str;
 
    });
    }
    function getPhoto()
    {
      FB.api('/me/picture?type=normal', function(response) {
 
          var str="<br/><b>Pic</b> : <img src='"+response.data.url+"'/>";
          document.getElementById("status").innerHTML+=str;
 
    });
 
    }
    function Logout()
    {
        FB.logout(function(){document.location.reload();});
    }
 
var signin = {};






signin.loginfrompage = function (){
    signin.userName         =   $("#loginUserName").val();
    signin.userPassword     =   $("#loginPassword").val();
    signin.login(signin.userName,signin.userPassword );
}

signin.login = function(uname,pword){

// signin.userName 		= 	$("#loginUserName").val();
// signin.userPassword		=	$("#loginPassword").val();

    AWSCognito.config.region = 'us-east-1';
    AWSCognito.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: COGNITO_IDENTITY_POOL_ID // your identity pool id here
    }); 

    // Need to provide placeholder keys unless unauthorised user access is enabled for user pool
    AWSCognito.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'})

    var authenticationData = {
        Username : uname , //signin.userName,
        Password : pword, //signin.userPassword,
    };
    var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
    var poolData = { 
        UserPoolId : USER_POOL_ID,
        ClientId :   APP_CLIENT_ID
    };
    var userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);
    var userData = {
        Username : uname, //signin.userName,
        Pool : userPool
    };
    var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
           
            var accesstoken = result.getIdToken().getJwtToken();
           
            var data = {};
                    data.title = "accesstoken";
                    data.token = result.getIdToken().getJwtToken();
                    data.displayName = cognitoUser.getUsername();
                    data.emails = [{}];
            cognitoUser.getUserAttributes(function(err, result) {
                if (err) {
                    alert(err);
                    return;
                }   
                    for (i = 0; i < result.length; i++) {
                        if(result[i].getName() == 'email')
                            {
                                data.emails[0].value = result[i].getValue(); 
                            }
                        //console.log('attribute ' + result[i].getName() + ' has value ' + result[i].getValue());
                    } 

                    $.ajax({
                        type: 'POST',
                        data: JSON.stringify(data),
                        contentType: 'application/json',
                        url: '/token',                      
                        success: function(data) {
                            window.location = "/showData";
                        }
                    });
            });
                   
        },

        onFailure: function(err) {
            alert(err);
        },

    });

};



//Required modules and libraries
var express = require('express'); //Web Application Framework
var path = require('path'); //handling and transforming file paths
var passport = require('passport'); //Authenticating middleware
var session = require('express-session'); //To track use sessions
var FacebookStrategy = require('passport-facebook').Strategy; //To authenticate facebook login
var AmazonStrategy = require('passport-amazon').Strategy;
var TwitterStrategy  = require('passport-twitter').Strategy;
var GoogleStrategy   = require('passport-google-oauth').OAuth2Strategy;
var util = require('util'); //Supports Node's internal API's
var AWS = require('aws-sdk'); //Amazon web Services SDK
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');


//Declaration of variables for the app
var cognitosync;
var cognitoidentity;

var THE_TITLE = "Cognito Sample App";


//Declaration of all properties linked to the environment (beanstalk configuration)
var AWS_ACCOUNT_ID = process.env.AWS_ACCOUNT_ID;
var AWS_REGION = process.env.AWS_REGION;
var COGNITO_IDENTITY_POOL_ID = process.env.COGNITO_IDENTITY_POOL_ID;
var IAM_ROLE_ARN = process.env.IAM_ROLE_ARN;
var COGNITO_DATASET_NAME = process.env.COGNITO_DATASET_NAME;
var COGNITO_KEY_NAME = process.env.COGNITO_KEY_NAME;
var COGNITO_KEY_NAME2 = process.env.COGNITO_KEY_NAME2;
var FACEBOOK_CALLBACKURL = process.env.FACEBOOK_CALLBACKURL; // For redirection after authentication
var FACEBOOK_CLIENT_ID = process.env.FACEBOOK_CLIENT_ID; // Fcebook App ID
var FACEBOOK_CLIENT_SECRET = process.env.FACEBOOK_CLIENT_SECRET; //Facebook App Secret
var AMAZON_CALLBACKURL = process.env.AMAZON_CALLBACKURL;
var AMAZON_CLIENT_ID = process.env.AMAZON_CLIENT_ID;
var AMAZON_CLIENT_SECRET = process.env.AMAZON_CLIENT_SECRET;
var TWITTER_CALLBACKURL = process.env.TWITTER_CALLBACKURL;
var TWITTER_CONSUMER_KEY = process.env.TWITTER_CONSUMER_KEY;
var TWITTER_CONSUMER_SECRET = process.env.TWITTER_CONSUMER_SECRET;
var GOOGLE_CALLBACKURL = process.env.GOOGLE_CALLBACKURL;
var GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID;
var GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET
var USER_POOL_ID = process.env.USER_POOL_ID;
var APP_CLIENT_ID = process.env.APP_CLIENT_ID;
var COGNITO_USER_POOL_ID = process.env.COGNITO_USER_POOL_ID; 
var ACCESS_KEY_ID = process.env.ACCESS_KEY_ID;
var SECRET_ACCESS_KEY = process.env.SECRET_ACCESS_KEY;


// var AWS_ACCOUNT_ID = '980592491844' ;//process.env.AWS_ACCOUNT_ID;  //AWS Acoount number 
// var AWS_REGION = 'us-east-1' ;//process.env.AWS_REGION; //Region where Cognito User Pool is created
// var COGNITO_IDENTITY_POOL_ID = 'us-east-1:1e507257-7a9f-4a0b-b50e-bbdfb6fb43a9' ;// process.env.COGNITO_IDENTITY_POOL_ID; 
// var IAM_ROLE_ARN = 'arn:aws:iam::980592491844:role/Cognito_ACTLoginAuth_Role' ;//process.env.IAM_ROLE_ARN; //$ aws iam get-role --role-name <role name>
// var COGNITO_DATASET_NAME = 'GAME' ;//process.env.COGNITO_DATASET_NAME; //Table where user data can be stored. Created if not present.
// var COGNITO_DATASET_NAME2 = 'PROVIDERS' ;
// var COGNITO_KEY_NAME = 'LIFE' ;//process.env.COGNITO_KEY_NAME; //Key in which data is stored JSON format
// var COGNITO_KEY_NAME2 = 'NAME' ;
// var COGNITO_KEY_NAME3 = 'PROVIDER' ;
// var FACEBOOK_CALLBACKURL = 'http://localhost:8080/auth/facebook/callback' ;//process.env.CALLBACKURL; // For redirection after authentication
// var FACEBOOK_CLIENT_ID = '107168556377898' ;//process.env.FACEBOOK_CLIENT_ID; // Fcebook App ID
// var FACEBOOK_CLIENT_SECRET = '9e84799b38a31710ae0895bd491b6c78' ;//process.env.FACEBOOK_CLIENT_SECRET; //Facebook App Secret
// var AMAZON_CALLBACKURL = 'http://localhost:8080/auth/amazon/callback' ; // process.env.CALLBACKURL;
// var AMAZON_CLIENT_ID = 'amzn1.application-oa2-client.1ce9650a8f0243b4b4c66f0a87621eaa'; //process.env.AMAZON_CLIENT_ID;
// var AMAZON_CLIENT_SECRET = '510da34bb40cefcd5bd209a63233b884bac56e45750f85684d304185baf8eebb'; //process.env.AMAZON_CLIENT_SECRET;
// var TWITTER_CALLBACKURL = 'http://localhost:8080/auth/twitter/callback' ;// process.env.CALLBACKURL;
// var TWITTER_CONSUMER_KEY = 'BPNSlOfBNu54Fzr4akfob7hPt'; //process.env.AMAZON_CLIENT_ID;
// var TWITTER_CONSUMER_SECRET = 'nAuEvTofvR0p6JSfnErTbgpt2K1LXQ1EVoznHeNWkpF5MjjYM8';
// var GOOGLE_CALLBACKURL = 'http://localhost:8080/auth/google/callback' ;// process.env.CALLBACKURL;
// var GOOGLE_CLIENT_ID = '711173193021-ef5t65t38q31m3srdjd2vvol4klpkpvl.apps.googleusercontent.com'; //process.env.AMAZON_CLIENT_ID;
// var GOOGLE_CLIENT_SECRET = 'oYo2fneTkMSvAgeobz7QeAJz';
// var USER_POOL_ID = 'us-east-1_qJfxkrrhP';
// var APP_CLIENT_ID = '7t652m50er0ne4d8kfa5fjrt5k';
// var COGNITO_USER_POOL_ID = 'cognito-idp.us-east-1.amazonaws.com/us-east-1_qJfxkrrhP';
// var ACCESS_KEY_ID = 'AKIAJ57NXGP3ARZO53TA';
// var SECRET_ACCESS_KEY = '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';



//Passport Serialization 
//This function is used to save user data in the session 
passport.serializeUser(function(user, done){
    done(null,user);
});


//Passport Deserialization 
//This function is used to retrieve user data from the session
passport.deserializeUser(function(obj,done){
    done(null,obj);
});

//Using the Amazon strategy to "Login with Amazon"
passport.use(new AmazonStrategy({
    clientID: AMAZON_CLIENT_ID,
    clientSecret: AMAZON_CLIENT_SECRET,
    callbackURL: AMAZON_CALLBACKURL
}, function(accessToken, refreshToken, profile, done) {
    process.nextTick(function() {
        profile.token = accessToken;
        profile.provider = "www.amazon.com";
        var user = profile;
        done(null, user);
    });
}));

//Using Facebook Strategy to login with Facebook

passport.use(new FacebookStrategy({
    clientID : FACEBOOK_CLIENT_ID,
    clientSecret : FACEBOOK_CLIENT_SECRET,
    callbackURL : FACEBOOK_CALLBACKURL,

     //passReqToCallback: true,
     profileFields : ['id', 'displayName', 'emails', 'photos']
},
function(accessToken, refreshToken, profile, done){
    process.nextTick( function(){
            profile.token = accessToken;
            profile.provider = 'graph.facebook.com';
            var user = profile;         
            done(null, user);
          });
}));

 passport.use(new TwitterStrategy({

        consumerKey     : TWITTER_CONSUMER_KEY,
        consumerSecret  : TWITTER_CONSUMER_SECRET,
        callbackURL     : TWITTER_CALLBACKURL,
        includeEmail: true
    },
    function(token, tokenSecret, profile, done) {
        process.nextTick( function(){
            profile.token = token + ";" + tokenSecret;
            profile.provider = 'api.twitter.com';
            var user = profile;
            done(null, user);
          });
}));

 passport.use(new GoogleStrategy({
    clientID : GOOGLE_CLIENT_ID,
    clientSecret : GOOGLE_CLIENT_SECRET,
    callbackURL : GOOGLE_CALLBACKURL,

     
},
function(token, refreshToken, idtoken ,profile, done){
    process.nextTick( function(){
            profile.token = idtoken.id_token;                                                                                                                                                                                                                                                                                                                                                
            profile.provider = 'accounts.google.com';                   
            var user = profile;         
            done(null, user);
          });
}));


//Initialize express
var app = express();

// setup of the app (view,assets,cookies,...)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret: 'foo',resave: true,saveUninitialized: true,cookie: {expires: false}}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());


//Initialize passport
app.use(passport.initialize());
app.use(passport.session());


//GET Home page
app.get('/', function(req, res){
    if(req.session.user)
    {
        res.redirect('/showdata'); 
    }
         
     AWS.config.region = AWS_REGION;
    //AWSCognito.config.region = AWS_REGION;
    res.render('login', {
        title: THE_TITLE,
        displayButtons: false
    });
});                                                                                                                                                                                                                                                                                                                                                                                                                                              

 app.get('/data', function(req, res){
    if(req.session.user)
    {
    res.render('info', {
        title: THE_TITLE,
        provider: req.session.user.provider,
        token: req.session.user.token
    });
    }
    else
    {
        res.redirect('/showdata'); 
    }

});  

//GET Logout page
app.get('/logout', function(req, res){
    req.session.isLoggedIn = false;
    delete req.session.user;
    req.logout();
    res.redirect('/');
});


/* GET Amazon page for authentication. */
app.get('/auth/amazon',
    passport.authenticate('amazon', {
        scope: ['profile']
    }),
    function(req, res) {
        // The request will be redirected to Amazon for authentication, so this
        // function will not be called.
});


/* GET Amazon callback page. */
app.get('/auth/amazon/callback', passport.authenticate('amazon', {
        failureRedirect: '/'
    }),
   function(req, res) {
        if(req.session.user)
        {
            res.redirect('/connect'); 
        }
        else{
            res.redirect('/showData');   
        }     
});


//GET facebook page for authentication
app.get('/auth/facebook',
    passport.authenticate('facebook', {
        scope: ['email']
    }),
    function(req, res) {
        
        // The request will be redirected to Facebook for authentication, so this
        // function will not be called.
});

//app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));


//Get Facebook callback page
app.get('/auth/facebook/callback', passport.authenticate('facebook', {
        failureRedirect: '/'
    }),
    function(req, res) {
        if(req.session.user) //If already a user is logged in
        {
            res.redirect('/connect'); 
        }
        else{
            res.redirect('/showData');   
        }  
});


app.get('/auth/twitter',
    passport.authenticate('twitter', {
        scope: ['profile']
    }),
    function(req, res) {
        // The request will be redirected to Amazon for authentication, so this
        // function will not be called.
});


/* GET twitter callback page. */
app.get('/auth/twitter/callback', passport.authenticate('twitter', {
        failureRedirect: '/'
    }),
    function(req, res) {
        if(req.session.user) //If already a user is logged in
        {
            res.redirect('/connect'); 
        }
        else{
            res.redirect('/showData');   
        }
});


app.get('/auth/google',
    passport.authenticate('google', {
        scope: ['profile']
    }),
    function(req, res) {
        // The request will be redirected to Amazon for authentication, so this
        // function will not be called.
});


/* GET Amazon callback page. */
app.get('/auth/google/callback', passport.authenticate('google', {
        failureRedirect: '/'
    }),
   function(req, res) {
        if(req.session.user) //If already a user is logged in
        {
            res.redirect('/connect'); 
        }
        else{
            res.redirect('/showData');   
        }        
});
    


//GET ShowData page
//This page initialize the CognitoID and the cognito client, then list the data comtained in the Cognito dataset
app.get('/showData', ensureAuthenticated, function(req, res){
    
    AWS.config.region = AWS_REGION;
    var params = {
        AccountId : AWS_ACCOUNT_ID,
        RoleArn : IAM_ROLE_ARN,
        IdentityPoolId : COGNITO_IDENTITY_POOL_ID,
        Logins: {
            'graph.facebook.com' : 'FBTOKEN',
            'www.amazon.com': 'AMAZONTOKEN',
            'accounts.google.com': 'GOOGLETOKEN',
            'api.twitter.com': 'TWITTERTOKEN'
        }
    };

    //initialze the Credential object

        AWS.config.credentials = new AWS.CognitoIdentityCredentials(params);
        AWS.config.credentials.params.Logins = {};
        AWS.config.credentials.params.Logins[req.session.user.provider] = req.session.user.token ;



    //GEt the credentials for the user
    AWS.config.credentials.get(function(err){
        if(err) console.log("## credentials.get: " .red + err, err.stack); //an error occured 
        else {
            
            req.session.user.COGNITO_IDENTITY_ID = AWS.config.credentials.identityId;
             
            //Other AWS SDKs will automatically use the cognito Credentials provided
            //configured in the JavaScript SDK.
            cognitosync = new AWS.CognitoSync();

            cognitosync.listRecords ({
                DatasetName: COGNITO_DATASET_NAME,
                IdentityId: AWS.config.credentials.identityId,
                IdentityPoolId: COGNITO_IDENTITY_POOL_ID 
            }, function(err, data){

                if(err) console.log("## listRecords: ".red + err, err.stack);
                else{
                    //Retrieve dataset metadata and SyncsessionToken for subsequent calls
                    req.session.user.COGNITO_SYNC_TOKEN = data.SyncSessionToken;
                    req.session.user.COGNITO_SYNC_COUNT = data.DatasetSyncCount;

                    //Check the existance of the key in the dataset
                    if(data.Count != "0") req.session.user.CURRENT_LIFE = data.Records[0].Value;
                    else req.session.user.CURRENT_LIFE = "0";

                    //Retrieve information on the dataset
                    var dataRecords = JSON.stringify(data.Records);

                    res.render('index' ,{
                        title: THE_TITLE,
                        gaugeValue: req.session.user.CURRENT_LIFE,
                        records: dataRecords,
                        Name : req.session.user.displayName,
                        Email: req.session.user.emails[0].value,
                        //displayButtons: req.user,
                        cognitoId: req.session.user.COGNITO_IDENTITY_ID

                    });
                }
            });
        }
    });

});


//GET /modifiedData
//This function will modify the data 
//Modified data is passed in the url

app.get('/modifyLife',ensureAuthenticated, function(req, res, next) {
    //Retrieve points from the URL parameter
    var points = parseInt(req.query.points);
    //Call to List Records in order to retrieve a new sync session token
    cognitosync.listRecords({
        DatasetName: COGNITO_DATASET_NAME, 
        IdentityId: req.session.user.COGNITO_IDENTITY_ID, 
        IdentityPoolId: COGNITO_IDENTITY_POOL_ID 
    }, function(err, data) {

        if (err) console.log("## listRecords: ".red + err, err.stack); // an error occurred
        else {
            //Retrieve dataset metadata and SyncSessionToken for subsequent calls
            req.session.user.COGNITO_SYNC_TOKEN = data.SyncSessionToken;
            req.session.user.COGNITO_SYNC_COUNT = data.DatasetSyncCount;

            //Compute current life, enforce a scale from 0 to 100 for the gauge
            req.session.user.CURRENT_LIFE = (parseInt(req.session.user.CURRENT_LIFE) + points).toString();
            if (parseInt(req.session.user.CURRENT_LIFE) < 0) req.session.user.CURRENT_LIFE="0";
            else if (parseInt(req.session.user.CURRENT_LIFE) > 100) req.session.user.CURRENT_LIFE="100";

            //Parameters for updating the dataset
            var params = {
                DatasetName: COGNITO_DATASET_NAME, 
                IdentityId: req.session.user.COGNITO_IDENTITY_ID, 
                IdentityPoolId: COGNITO_IDENTITY_POOL_ID, 
                SyncSessionToken: req.session.user.COGNITO_SYNC_TOKEN, 
                RecordPatches: [{
                    Key: COGNITO_KEY_NAME, 
                    Op: 'replace', 
                    SyncCount: req.session.user.COGNITO_SYNC_COUNT, 
                    Value: req.session.user.CURRENT_LIFE
                },{
                    Key: COGNITO_KEY_NAME2, 
                    Op: 'replace', 
                    SyncCount: req.session.user.COGNITO_SYNC_COUNT, 
                    Value: req.session.user.displayName
                }
                ]
            };


            //Make the call to Amazon Cognito
            cognitosync.updateRecords(params, function(err, data) {
                if (err) {
                    console.log("## updateRecords: ".red + err, err.stack);
                } // an error occurred
                else {
                    var dataRecords = JSON.stringify(data);
                    //render the page
                    res.render('index', {
                        title: THE_TITLE,
                        gaugeValue: req.session.user.CURRENT_LIFE,
                        records: dataRecords,
                        Name: req.session.user.displayName,
                        Email: req.session.user.emails[0].value,
                        //displayButtons: req.user,
                        cognitoId: req.session.user.COGNITO_IDENTITY_ID
                    });
                }
            });

        }
    });
});

/*app.get('/connect',ensureAuthenticated, function(req, res, next) {


AWS.config.region = AWS_REGION;
    var params = {
        AccountId : AWS_ACCOUNT_ID,
        RoleArn : IAM_ROLE_ARN,
        IdentityPoolId : COGNITO_IDENTITY_POOL_ID,
        Logins: {
            'graph.facebook.com' : 'FBTOKEN',
            'www.amazon.com': 'AMAZONTOKEN',
            'accounts.google.com': 'GOOGLETOKEN',
            'api.twitter.com': 'TWITTERTOKEN'
        }
    };

    //initialze the Credential object

        AWS.config.credentials = new AWS.CognitoIdentityCredentials(params);
        AWS.config.credentials.params.Logins = {};
        AWS.config.credentials.params.Logins[req.session.user.provider] = req.session.user.token ;
        if( req.user != null)
        {
            AWS.config.credentials.params.Logins[req.user.provider] = req.user.token ;
        }

    //GEt the credentials for the user
    AWS.config.credentials.get(function(err){
        if(err) console.log("## credentials.get: " .red + err, err.stack); //an error occured 
        else {
            
            req.session.user.COGNITO_IDENTITY_ID = AWS.config.credentials.identityId;
            res.redirect('/services');
            
        }
    });
    



});
*/

app.get('/connect',ensureAuthenticated, function(req, res, next) {


        var ciparams = {
            region: AWS_REGION
        };
        
        cognitoidentity = new AWS.CognitoIdentity(ciparams);
        var params = {
          IdentityPoolId: COGNITO_IDENTITY_POOL_ID, /* required */
          AccountId : AWS_ACCOUNT_ID 
        };

        
        params.Logins = {};
        params.Logins[req.session.user.provider] = req.session.user.token ;
        if( req.user != null)
        {
            params.Logins[req.user.provider] = req.user.token ;
        }

    //GEt the credentials for the user
        cognitoidentity.getId(params, function(err, data) {
            if(err) console.log("## credentials.get: " .red + err, err.stack); //an error occured 
            else {              // successful response
                req.session.user.COGNITO_IDENTITY_ID = data.IdentityId;
                res.redirect('/services');
                
            }          
        });

});



//Using from the client side
/*app.get('/signUp', function(req, res, next){
        //AWS.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId : COGNITO_IDENTITY_POOL_ID});
        //AWS.config.region = AWS_REGION;
        var cispparams = {
            region: 'us-east-1'
        };
        var cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider(cispparams);

      
        //var cognitoUser;
        //AWS.CognitoIdentityCredentials.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId : COGNITO_IDENTITY_POOL_ID});
        AWS.config.update({accessKeyId: 'anything', secretAccessKey: 'anything'});
        var params = {
            ClientId: APP_CLIENT_ID, // required 
            Password: ' ', // required 
            Username: ' ', // required 
            //SecretHash: 'STRING_VALUE', App client secret add later
            UserAttributes: [
            {
              Name: 'email', // required /
              Value: ' '
            }
            // more items /
            ],
            ValidationData: [
                {
                  Name: 'STRING_VALUE', //required 
                  Value: 'STRING_VALUE'
                },
                // more items 
              ]
            };
        cognitoidentityserviceprovider.signUp(params, function(err, result){
        if (err) {
            console.log(err);
            return;
        }
        else{
            //cognitoUser = result.user;
            console.log('User Created!');
            AWS.config.credentials.get(function(err){
                if(err) console.log("## credentials.get: " .red + err, err.stack); //an error occured 
                else {
                    

            //Admin Confirm with COnfirmation code
            var acsuparams = {
              UserPoolId: USER_POOL_ID, // required 
              Username: ' ' // required 
            };
            cognitoidentityserviceprovider.adminConfirmSignUp(acsuparams, function(err, data) {
            if (err) console.log("## adminConfirmSignUp: " .red + err, err.stack); // an error occurred
            else     {
            console.log(data); 
            console.log('User Confirmed!');
            }          // successful response
            });

            
            //Confirmation with code sent to email or phone number
              var params = {
              ClientId: APP_CLIENT_ID, // required 
              ConfirmationCode: ' ', //required 
              Username: ' ', //required 
              ForceAliasCreation: true //|| false
              //SecretHash: 'STRING_VALUE'
            };
            cognitoidentityserviceprovider.confirmSignUp(params, function(err, data) {
              if (err) console.log(err, err.stack); // an error occurred
              else     console.log(data);           // successful response
            });


        }
    })
        }
            
    }); 

});*/


app.post('/token', function(req, res){
    var obj = {};
    var user =req.body; 
   
    user.provider = COGNITO_USER_POOL_ID;
    if(req.session.user) //If user already exists and connecting another ID
    {   

    }
    else{
        req.session.user = user;
        req.session.isLoggedIn = true;
        req.session.save();
    }
    res.redirect('/showData');  
    
});




app.get('/services', function(req, res){

            AWS.config.region = AWS_REGION;
            var params = {
                IdentityPoolId : COGNITO_IDENTITY_POOL_ID
            };

            //initialze the Credential object

                AWS.config.credentials = new AWS.CognitoIdentityCredentials(params);
                AWS.config.credentials.params.Logins = {};
                AWS.config.credentials.params.Logins[req.session.user.provider] = req.session.user.token ;


            //GEt the credentials for the user
            AWS.config.credentials.get(function(err){
                if(err) console.log("## credentials.get: " .red + err, err.stack); //an error occured 
                else {
                    
                    //req.session.user.COGNITO_IDENTITY_ID = AWS.config.credentials.identityId;
                    //AWS.CognitoIdentityCredentials.config.credentials = new AWS.CognitoIdentityCredentials({IdentityPoolId : COGNITO_IDENTITY_POOL_ID});
                    AWS.config.update({accessKeyId: ACCESS_KEY_ID, secretAccessKey: SECRET_ACCESS_KEY});

                    cognitoidentity = new AWS.CognitoIdentity({region: AWS_REGION});

                    var params = {
                      IdentityId: req.session.user.COGNITO_IDENTITY_ID 
                    };
                    cognitoidentity.describeIdentity(params, function(err, data) {
                      if (err) console.log(err, err.stack); // an error occurred
                      else     { 
                              
                            var fbdb= false;
                            var tdb= false;
                            var adb=false;
                            var gdb= false;  
                            for(var i = 0; i < data.Logins.length; i++) {
                                 if(data.Logins[i] == 'graph.facebook.com')
                                 {
                                    fbdb = true;
                                 }
                                 else if(data.Logins[i]== 'api.twitter.com')
                                 {
                                    tdb = true;
                                 }
                                 else if(data.Logins[i] == 'www.amazon.com')
                                 {
                                    adb = true;
                                 }
                                 else if(data.Logins[i] == 'accounts.google.com')
                                 {
                                    gdb = true;
                                 }
                            }      
                         res.render('connect' ,{
                                title: THE_TITLE,
                                records: JSON.stringify(data),
                                Name : req.session.user.displayName,
                                Email: req.session.user.emails[0].value,
                                Displayfb : fbdb,
                                Displayt : tdb,
                                Displaya : adb,
                                Displayg : gdb,
                                cognitoId: req.session.user.COGNITO_IDENTITY_ID

                            });
                      }          
                    });
                }
            });
       
});   






app.get('/disconnect/facebook', ensureAuthenticated, function(req, res){


        var cispparams = {
            region: AWS_REGION
        };
        
        cognitoidentity = new AWS.CognitoIdentity(cispparams);
        var params = {
          IdentityId:  req.session.user.COGNITO_IDENTITY_ID, /* required */
           Logins: { /* required */
              
          },
          LoginsToRemove: [ /* required */
            'graph.facebook.com'
          ]
        };
        params.Logins = {};
        params.Logins[req.session.user.provider] = req.session.user.token ;

        cognitoidentity.unlinkIdentity(params, function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else     {  // successful response
                res.redirect('/services');
          }           
        });
});

app.get('/disconnect/twitter', ensureAuthenticated, function(req, res){


        var cispparams = {
            region: AWS_REGION
        };
        cognitoidentity = new AWS.CognitoIdentity(cispparams);
        var params = {
          IdentityId:  req.session.user.COGNITO_IDENTITY_ID, /* required */
           Logins: { /* required */
             'api.twitter.com' : req.session.user.token
          },
          LoginsToRemove: [ /* required */
            'api.twitter.com'
          ]
        };
        params.Logins = {};
        params.Logins[req.session.user.provider] = req.session.user.token ;

        cognitoidentity.unlinkIdentity(params, function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else     {  // successful response
                res.redirect('/services');
          }           
        });
});

app.get('/disconnect/amazon', ensureAuthenticated, function(req, res){


        var cispparams = {
            region: AWS_REGION
        };

        cognitoidentity = new AWS.CognitoIdentity(cispparams);
        var params = {
          IdentityId:  req.session.user.COGNITO_IDENTITY_ID, /* required */
           Logins: { /* required */
              
          },
          LoginsToRemove: [ /* required */
            'www.amazon.com'
          ]
        };
        params.Logins = {};
        params.Logins[req.session.user.provider] = req.session.user.token ;

        cognitoidentity.unlinkIdentity(params, function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else     {  // successful response
                res.redirect('/services');
          }           
        });
});

app.get('/disconnect/google', ensureAuthenticated, function(req, res){


        var cispparams = {
            region: AWS_REGION
        };

        cognitoidentity = new AWS.CognitoIdentity(cispparams);
        var params = {
          IdentityId:  req.session.user.COGNITO_IDENTITY_ID, /* required */
           Logins: { /* required */
             
          },
          LoginsToRemove: [ /* required */
            'accounts.google.com'
          ]
        };
        params.Logins = {};
        params.Logins[req.session.user.provider] = req.session.user.token ;

        cognitoidentity.unlinkIdentity(params, function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else     {  // successful response
                res.redirect('/services');
          }           
        });
});


 
/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// Error handler, Stacktrace is displayed
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: err
    });
});




//Simple route middleware to ensure user is authenticated. Use this route middleware on any resource that needs to be protected.
//If the request is authenticated (via a persistent login session),
//the request will proceed.  Otherwise, the user will be redirected to the home page for login.
function ensureAuthenticated(req, res, next) {
    if (req.session.isLoggedIn) 
        { 
            return next(); 
        }
    if (req.isAuthenticated()) 
        { 
        req.session.user = req.user; 
        req.session.isLoggedIn = true;
        delete req.user;
        return next(); 
        }
  res.redirect('/');
}

//Set the port of the app
app.set('port', process.env.PORT || 8080);

//Launch the express server
var server = app.listen(app.get('port'), function() {
  console.log('Express server listening on port ' + server.address().port);
});


































